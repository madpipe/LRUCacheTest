/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lrucache.h"
#include <stdio.h>

LRUCache::LRUCache()
{
    for (int i = 0; i < maxItems; i++)
    {
        items[i] = 0;
    }
    size = 0;
}

LRUCache::~LRUCache()
{
    for (int i = 0; i < maxItems; i++)
    {
        if (items[i] != 0)
            delete items[i];
    }
}

std::string LRUCache::get(short int key)
{
    for (int i = 0; i < maxItems; i++)
    {
        if (items[i]->key == key)
        {
            Item* item = items[i];

            // Remove item from order
            item->next->previous = item->previous;
            item->previous->next = item->next;

            // Set item as first
            item->next = first;
            first->previous = item;
            first = item;

            return item->value;
        }
    }
    return "";
}

void LRUCache::set(int key, std::string value)
{
    // Store first empty entry found.
    int firstEmpty = -1;

    for (int i = 0; i < maxItems; i++)
    {
        // Check if key exists
        if (items[i] != 0)
        {
            Item* item = items[i];
            if (item->key == key)
            {
                // Update cache entry
                item->value = value;

                // Remove item from order
                if (item != last)
                {
                    item->next->previous = item->previous;
                    item->previous->next = item->next;
                }
                else
                {
                    item->previous->next = 0;
                    last = item->previous;
                }

                // Set item as first
                item->next = first;
                first->previous = item;
                first = item;

                return;
            }
        }
        else
        {
            if (firstEmpty == -1)
                firstEmpty = i;
        }
    }

    // If cache not full yet
    if (size < maxItems)
    {
        // Should never fail here
        // If cache is not full, an empty slot must have been found.
        if (firstEmpty != -1)
        {
            Item* item = new Item;
            item->key = key;
            item->value = value;
            item->next = first;
            item->previous = 0;

            // Set this new item as first
            if (size > 0)
                first->previous = item;
            first = item;

            // Add item in first empty slot found;
            items[firstEmpty] = item;
            size++;

            if (size == 1)
                last = item;

            return;
        }
        else
        {
            // 404
        }
    }
    // Cache is full
    else
    {
        // Reset last as new item
        Item* item = last;
        item->key = key;
        item->value = value;

        // Update last
        last = last->previous;

        // Set new item as first
        item->next = first;
        item->previous = 0;
        first->previous = item;
        first = item;
    }
}

void LRUCache::printCache()
{
    Item* item = first;
    for (int i = 0; i < size; i++)
    {
        printf("Order: %d\tKey: %d\tValue: %s\n",i, item->key, item->value.c_str());
        if (item != last)
            item = item->next;
    }
    printf("---------------------------------------------\n");
}
