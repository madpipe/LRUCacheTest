/*
 * LRU Cache class. Container class for the cache items.
 *
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LRUCACHE_H
#define LRUCACHE_H

#include <vector>
#include <string>

class LRUCache
{
public:
    LRUCache();
    ~LRUCache();

    std::string get(short int key);
    void set(int key, std::string value);

    void printCache();

private:

    struct Item{
        Item* previous;
        Item* next;
        int key;
        std::string value;
    };

    static const short int maxItems = 10;
    Item* items[maxItems];

    Item* first;
    Item* last;
    unsigned int size;
};

#endif // LRUCACHE_H
