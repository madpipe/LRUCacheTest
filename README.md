LRU (Least Recently Used) type cache sample application.
=========================================================

Requirements
---------------------
Create a "least recently used" cache class in one of either PHP, Python or C++. The class should have the following specification:
 1. Two public methods get($key) and set($key, $value) (obviously as many private/protected methods are required).
 2. Allow up to 10 items to be in the cache at all times. Evicting the least recently used items.
 3. Store cached items persistently across requests, using either MySQL table, APC, Memecache, Redis or file system.
 4. Bear in mind it's a cache so it should be fast and thus use a fast cross-request cache/storage engine, optimised for a large volume of hits.

------

Up to the current state I haven't done any reasearch on the topic.

Notes
-------

* For caches, a persistent storage on the HDD would drasticly reduce it's speed. That is why I have not implemented that requirement. I can implement a "backup" method that, on request, will store the current cache state in a storage, but that would not be persistent.

* The requirements do not specify that the cache would be used in a multithreaded environment, so support for that was not added.

* The requirements do not specify what type the value is, so I currently implemented it with std::string.

* The requirements do not specify what is the key is. The key could be an integer, an index, a memory location, a string, etc. If the key would be a memory location, finding the desired item would be faster.

* I chose C++ because I'm most confortable with it.
