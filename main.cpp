/*
 * LRU Cache test application.
 *
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include "lrucache.h"

void print(std::string str)
{
    std::cout << str << std::endl;
}

int main(int argc, char **argv) {
    LRUCache lruc;

    lruc.set(2, "foo");
    lruc.printCache();

    lruc.set(3, "bar");
    lruc.printCache();

    lruc.set(2, "baruba");
    lruc.printCache();

    lruc.set(1, "f1");
    lruc.printCache();

    lruc.set(2, "f2");
    lruc.printCache();

    lruc.set(3, "f3");
    lruc.printCache();

    lruc.set(4, "f4");
    lruc.printCache();

    lruc.set(5, "f5");
    lruc.printCache();

    lruc.set(6, "f6");
    lruc.printCache();

    lruc.set(7, "f7");
    lruc.printCache();

    lruc.set(8, "f8");
    lruc.printCache();

    lruc.set(9, "f9");
    lruc.printCache();

    lruc.set(10, "f10");
    lruc.printCache();

    lruc.set(3, "foo");
    lruc.printCache();

    lruc.set(11, "bar");
    lruc.printCache();

    lruc.get(5);
    lruc.printCache();


    return 0;
}
